package airlines;

public class Airlines {
    private String airlinesName;
    private String code;

    public Airlines(String airlinesName, String code) {
        this.airlinesName = airlinesName;
        this.code = code;
    }

    public String getAirlinesName() {
        return airlinesName;
    }

    public void setAirlinesName(String airlinesName) {
        this.airlinesName = airlinesName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
