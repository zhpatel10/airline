package airlines;

public class Airplane {

    private String model;
    private String make;
    private int numOfSeats;
    private int id;

    public Airplane(String model, String make, int numOfSeats, int id) {
        this.model = model;
        this.make = make;
        this.numOfSeats = numOfSeats;
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public int getNumOfSeats() {
        return numOfSeats;
    }

    public void setNumOfSeats(int numOfSeats) {
        this.numOfSeats = numOfSeats;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
