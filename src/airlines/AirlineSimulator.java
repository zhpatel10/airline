package airlines;

public class AirlineSimulator {
    public static void main(String[] args)
    {
        Flight f1= new Flight("1245","JAN 20");

        Airport air1= new Airport("DLH123","Delhi")  ;

        Airlines line1=new Airlines("Jet Airways","JET234");

        Airplane plane1=new Airplane("A1","India",100,234);

        System.out.println("Passenger with flight number "+ f1.getNumber() + " is travelling from " + air1.getCity() +
                " whose code "+ air1.getAirportCode() + " on "+ f1.getDate() + " in " + line1.getAirlinesName() + " " + line1.getCode() + " airlines and code ");
        System.out.println("Airplane's model and make is " + plane1.getModel() + " " + plane1.getMake()+ " with "
                + plane1.getNumOfSeats() + " number of seats and id is " + plane1.getId());
    }
}
