package airlines;

public class Flight
{
        private String number;
        private String date;

        public Flight(String number, String date) {
            this.number = number;
           this.date = date;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
}

